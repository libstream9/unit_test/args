#include <stream9/args.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

using stream9::args;

namespace json { using namespace stream9::json; }

BOOST_AUTO_TEST_SUITE(args_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        args a;
    }

    BOOST_AUTO_TEST_CASE(value_constructor_)
    {
        char const* argv[] = { "foo", "bar", "baz", nullptr };
        int argc = 3;

        args a(argc, argv);

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(range_constructor_)
    {
        args a { "foo", "bar" };

        json::array expected { "foo", "bar" };
        BOOST_TEST(json::value_from(a) == expected);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_)
    {
        args a1 { "foo", "bar", "baz" };
        args a2(a1);

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a2) == expected);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_)
    {
        args a1 { "foo", "bar", "baz" };
        args a2 { "foo" };

        a2 = a1;

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a2) == expected);
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        args a1 { "foo", "bar", "baz" };
        args a2 (std::move(a1));

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a2) == expected);
    }

    BOOST_AUTO_TEST_CASE(move_assignment_)
    {
        args a1 { "foo", "bar", "baz" };
        args a2 { "123", "5563" };

        a2 = std::move(a1);

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a2) == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_)
    {
        args a1;
        a1.insert(a1.end(), "foo");
        a1.insert(a1.end(), std::string("bar"));

        json::array expected { "foo", "bar" };
        BOOST_TEST(json::value_from(a1) == expected);
    }

    BOOST_AUTO_TEST_CASE(range_insert_)
    {
        args a1;
        auto i1 = { "foo", "bar", "baz" };

        a1.insert(a1.end(), i1.begin(), i1.end());

        json::array expected { "foo", "bar", "baz" };
        BOOST_TEST(json::value_from(a1) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // args_

} // namespace testing
